#StudOrga - Android App

##General Information
StudOrga or Studium Organisator (engl. university/college organizer) is an application for android smartphones and tablets.
It was planned and created as an university project at the "Technische Hochschule Mittelhessen".
The goal for the app was to allow the user to manage their appointments, homework and lessons. A lot of optional features were planned but never implemented due to time limitations.

[![Get it on Google Play](https://developer.android.com/images/brand/en_generic_rgb_wo_45.png)](https://play.google.com/store/apps/details?id=de.frost.david.android.studorga)


![Picture of StudOrga in landscape mode](https://raw.githubusercontent.com/David-GER/StudOrga/gh-pages/images/studorga_land_vorlesungen_lr.png)
![Picture the navigation drawer in landscape mode](https://raw.githubusercontent.com/David-GER/StudOrga/gh-pages/images/studorga_land_vorlesungen_menu_lr.png)

##Technical Information
* The application uses a SQLite database as local storage for the user data.
* The main navigation to switch between the lesson, appointment and homework section is a navigation drawer.
* The app uses fragments
* With the help of the support library and ActionBarSherlock the app also works on android 2.3.

##Todo
As part of the planning process some features were planned as optional.
* Check via GPS, if the user is now at the university. Use pushup messages to inform the user about the lessons etc of the day
* The whole homework section is currently unimplemented
* The app should set the phone to silence at the beginning of a listed lesson
* A webserver backend. Students don't need to insert their timetable
* Being able to send important messages to users who have a specific lesson via the backend server
* Special app edition for teachers, lecturers, professors. More features like the ability to inform their students that a lesson is canceled.
* Manage files like scripts or pictures and be able to add them to lessons
* Upload files to google drive as backup and to share them with others
* Generate QR codes to files in google drive and display the qr codes on the screen. Other smartphone users can scan the code to download the file easily
* ...

##More pictures
![The lesson overview](https://raw.githubusercontent.com/David-GER/StudOrga/gh-pages/images/studorga_vorlesung_liste_lr.png)![Adding a new lesson](https://raw.githubusercontent.com/David-GER/StudOrga/gh-pages/images/studorga_veranstaltungen_neu_lr.png)
