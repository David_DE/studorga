package de.frost.david.android.studorga;

import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

public class TerminActivity extends SingleFragmentActivity {

	public static final String EXTRA_TERMIN_ID = "EXTRA_TERMIN_ID";

	@Override
	protected Fragment createFragment() {
		long tid = getIntent().getLongExtra(EXTRA_TERMIN_ID, -1);
		if (tid != -1) {
			return TerminFragment.newInstance(tid);
		}
		return new TerminFragment();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
