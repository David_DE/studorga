package de.frost.david.android.studorga;

import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.MenuItem;

import de.frost.david.android.studorga.R;
import de.frost.david.android.studorga.model.IUniDataModel;
import de.frost.david.android.studorga.model.Semester;
import de.frost.david.android.studorga.model.UniDataModelFactory;
import de.frost.david.android.studorga.model.UniDatabaseHelper.SemesterCursor;

public class EinstellungenPreferenceActivity extends PreferenceActivity {
	private IUniDataModel mDataModel;
	private SemesterCursor mSemesterCursor;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
//		getActionBar().setHomeButtonEnabled(true); TODO: fixxx
//		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		addPreferencesFromResource(R.xml.pref_layout);
		mDataModel = UniDataModelFactory.createInstance(this);
		
		PreferenceScreen root = getPreferenceManager().createPreferenceScreen(this);
		PreferenceCategory cat = new PreferenceCategory(this);
		cat.setTitle("Test Cat title");
		
		root.addPreference(cat);
		
		mSemesterCursor = mDataModel.getAlleSemester();
		mSemesterCursor.moveToFirst();
		
		CharSequence[] entries = new CharSequence[mSemesterCursor.getCount()];
		CharSequence[] values = new CharSequence[mSemesterCursor.getCount()];
		
		for (int i = 0; i < mSemesterCursor.getCount(); i++) {
			Semester s = mSemesterCursor.getSemester();
			
			entries[i] = s.getName();
			values[i] = ""+s.getId();
			mSemesterCursor.moveToNext();
		}
		
		ListPreference listPref = (ListPreference)findPreference("selectedSemester");
		listPref.setEntries(entries);
		listPref.setEntryValues(values);
		
		listPref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
			
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				Log.i("test", "Value: " + newValue.toString());
				setResult(RESULT_OK);
				return true;
			}
		});
		
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
