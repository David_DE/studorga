package de.frost.david.android.studorga;

import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

public class StundeActivity extends SingleFragmentActivity {

	public static final String EXTRA_STUNDE_ID = "EXTRA_STUNDE_ID";

	@Override
	protected Fragment createFragment() {
		long sid = getIntent().getLongExtra(EXTRA_STUNDE_ID, -1);
		if (sid != -1) {
			return StundeFragment.newInstance(sid);
		}
		return new StundeFragment();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
