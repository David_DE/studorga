package de.frost.david.android.studorga;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import de.frost.david.android.studorga.R;
import de.frost.david.android.studorga.model.IUniDataModel;
import de.frost.david.android.studorga.model.Termin;
import de.frost.david.android.studorga.model.UniDataModelFactory;
import de.frost.david.android.studorga.model.UniDatabaseHelper.TerminCursor;

/*
 * TerminListFragment ist eine einfache Liste, dessen Eintraege spaeter Termindaten beinhalten werden.
 */
public class TerminListFragment extends ListFragment {
	private IUniDataModel mDataModel;
	private TerminCursor mCursor;
	
	private int mSelectedItem = -1;
	private ActionMode mAction;
	
	private ActionMode.Callback mActionModeCallback = new ActionMode.Callback(){

	    @Override 
	    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
	          MenuInflater inflater = mode.getMenuInflater();
	          inflater.inflate(R.menu.fragment_context_terminlist, menu);
	          return true;
	        }

	    @Override
	    public void onDestroyActionMode(ActionMode mode) {
			TerminCursorAdapter a = (TerminCursorAdapter) getListAdapter();
			a.removeSelection();
			mAction = null;
	    }

	    @Override
	    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
	        switch (item.getItemId()) {
	            case R.id.context_menu_deleteTermin:
	            	TerminCursor tc = (TerminCursor)getListView().getItemAtPosition(mSelectedItem);
	            	
	            	//TODO: Undo möglichkeit?
//	            	Termin undo = tc.getTermin(); 
	            	
	            	if (mDataModel.deleteTermin(tc.getTermin().getId())) {
	            		Toast.makeText(getActivity(), getString(R.string.context_menu_terminlist_termin_geloescht), Toast.LENGTH_LONG).show();
	            	} else {
	            		Toast.makeText(getActivity(), getString(R.string.fehler_beim_loeschen), Toast.LENGTH_LONG).show();	            		
	            	}
	            	updateUI();
	            	mode.finish();
	                return true;
	            default:
	                mode.finish();
	                return false;
	       }
	        
	    }

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}
	};
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_terminliste, container, false);
		
		
		mDataModel = UniDataModelFactory.createInstance(getActivity());
		
		//TODO: Alte Termine ausblenden
//		mCursor = mDataModel.getAlleTermineAktuell();
		mCursor = mDataModel.getAlleTermine();
		TerminCursorAdapter adapter = new TerminCursorAdapter(getActivity(), mCursor);
		setListAdapter(adapter);
		updateUI();
		
		return v;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		getListView().setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				if (mAction != null) return false;
				
				mSelectedItem = position;
				TerminCursorAdapter a = (TerminCursorAdapter) getListAdapter();
				a.toggleSelection(mSelectedItem);
				mAction = ((ActionBarActivity)getActivity()).startSupportActionMode(mActionModeCallback);
				
				return true;
			}
		});
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
	    super.onActivityCreated(savedInstanceState);
	    getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
	}
	

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.fragment_menu_termine, menu);
	}
	
	@Override
    public void onListItemClick(ListView l, View v, int position, long id) {
		
		if (mAction == null) {
			Intent i = new Intent(getActivity(), TerminActivity.class);
			
			Termin t = ((TerminCursor)getListAdapter().getItem(position)).getTermin();
			long tid = t.getId();
			
			i.putExtra(TerminActivity.EXTRA_TERMIN_ID, tid);
			getActivity().startActivityForResult(i, StudOrgaActivity.REQUEST_TERMIN);
			
		} else {
			TerminCursorAdapter a = (TerminCursorAdapter) getListAdapter();
			a.removeSelection();
			mSelectedItem = position;
			a.toggleSelection(mSelectedItem);
		}
		
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_item_new_termin:
			getActivity().startActivityForResult(new Intent(getActivity(), TerminActivity.class), StudOrgaActivity.REQUEST_TERMIN);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	public void updateUI() {
		mCursor.requery();
		((TerminCursorAdapter)getListAdapter()).notifyDataSetChanged();
	}
	
	
	public static class TerminCursorAdapter extends CursorAdapter {
		
		private TerminCursor mTerminCursor;
		private SparseBooleanArray mSelectedItems; //TODO: ggf. aus array ein einfachern wert machen, da nur SingleSelection
//		private int mSelectedItem;

		public TerminCursorAdapter(Context context, TerminCursor c) {
			super(context, c, 0);
			this.mTerminCursor = c;
			mSelectedItems = new SparseBooleanArray();
//			mSelectedItem = -1;
		}
		
		public void toggleSelection(int position)
		{
		    selectView(position, !mSelectedItems.get(position));
//		    mSelectedItem = (mSelectedItem == position)? -1: position;
		}
		 
		public void removeSelection() {
			mSelectedItems = new SparseBooleanArray();
//			mSelectedItem = -1;
		    notifyDataSetChanged();
		}
		 
		public void selectView(int position, boolean value)
		{
		    if(value)
		    	mSelectedItems.put(position, value);
		    else
		    	mSelectedItems.delete(position);
		             
		    notifyDataSetChanged();
		}
		         
		public int getSelectedCount() {
		    return mSelectedItems.size();
		}
		         
		public SparseBooleanArray getSelectedIds() {
		    return mSelectedItems;
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			return inflater.inflate(R.layout.list_item_termin, parent, false);
		}
		
		@Override
		public void bindView(View v, Context c, Cursor cursor) {
			Termin t = mTerminCursor.getTermin();
			
			TextView tv_name = (TextView)v.findViewById(R.id.list_termin_textView_name);
			TextView tv_ort = (TextView)v.findViewById(R.id.list_termin_textView_ort);
			TextView tv_datum = (TextView)v.findViewById(R.id.list_termin_textView_datum);
			TextView tv_uhrzeit = (TextView)v.findViewById(R.id.list_termin_textView_uhrzeit);
			
			
			tv_name.setText(t.getName());
			tv_ort.setText(t.getOrt());
			tv_datum.setText(t.getDate());
			tv_uhrzeit.setText(t.getTime());
			
			v.setBackgroundColor(mSelectedItems.get(cursor.getPosition())? 0x9934B5E4: Color.TRANSPARENT);
//			v.setBackgroundColor((mSelectedItem == cursor.getPosition())? 0x9934B5E4: Color.TRANSPARENT);
		}
	}
}