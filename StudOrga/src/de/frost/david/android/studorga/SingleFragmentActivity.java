package de.frost.david.android.studorga;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import de.frost.david.android.studorga.R;


/*
 * SingleFragmentActivity stellt den Grundbaustein einer Aktivitaesklasse dar, die ein Fragment hosten soll.
 */
public abstract class SingleFragmentActivity extends ActionBarActivity {
	
	protected abstract Fragment createFragment();
	
	protected int getLayoutResId() {
		return R.layout.activity_fragment;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(getLayoutResId());
		
		// Der FragmentManager erledigt das wechseln von Fragmenten
		FragmentManager fm = getSupportFragmentManager();
		
		Fragment fragment = fm.findFragmentById(R.id.fragmentContainer);
		
		if (fragment == null) {
			fragment = createFragment();
			fm.beginTransaction().add(R.id.fragmentContainer, fragment).commit();
		}
	}

}
