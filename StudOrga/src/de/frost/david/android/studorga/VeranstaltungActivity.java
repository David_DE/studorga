package de.frost.david.android.studorga;

import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

public class VeranstaltungActivity extends SingleFragmentActivity {

	public static final String EXTRA_VERANSTALTUNG_ID = "EXTRA_VERANSTALTUNG_ID";

	@Override
	protected Fragment createFragment() {
		long vid = getIntent().getLongExtra(EXTRA_VERANSTALTUNG_ID, -1);
		if (vid != -1) {
			return VeranstaltungFragment.newInstance(vid);
		}
		return new VeranstaltungFragment();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
