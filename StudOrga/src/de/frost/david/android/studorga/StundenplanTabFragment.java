package de.frost.david.android.studorga;

import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import android.widget.Toast;
import de.frost.david.android.studorga.R;
import de.frost.david.android.studorga.model.IUniDataModel;
import de.frost.david.android.studorga.model.Stunde;
import de.frost.david.android.studorga.model.UniDataModelFactory;
import de.frost.david.android.studorga.model.UniDatabaseHelper.StundeCursor;

public class StundenplanTabFragment extends Fragment {
	private static long SELECTED_SEMESTER = -1;
	public static final String ARG_NUMBER = "number";

	private ListView mListViewMontag;
	private ListView mListViewDienstag;
	private ListView mListViewMittwoch;
	private ListView mListViewDonnerstag;
	private ListView mListViewFreitag;

	private StundeCursor mMontagsCursor;
	private StundeCursor mDienstagsCursor;
	private StundeCursor mMittwochsCursor;
	private StundeCursor mDonnerstagsCursor;
	private StundeCursor mFreitagsCursor;
	private IUniDataModel mDataModel;

	private int mSelectedItem = -1;
	private ActionMode mAction;

	// private mActionModeCallback = new ActionMode.Callback()

	private class MyActionThing implements ActionMode.Callback {
		private StundeCursorAdapter mAdapter;

		public MyActionThing(StundeCursorAdapter adapter) {
			this.mAdapter = adapter;
		}

		public StundeCursorAdapter getAdapter() {
			return mAdapter;
		}

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			MenuInflater inflater = mode.getMenuInflater();
			inflater.inflate(R.menu.fragment_context_stundenlisten, menu);
			return true;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			getAdapter().removeSelection();
			mAction = null;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			switch (item.getItemId()) {
			case R.id.context_menu_deleteStunde:
				StundeCursor sc1 = (StundeCursor) getAdapter().getItem(mSelectedItem);

				// TODO: Undo m�glichkeit?

				if (mDataModel.deleteStunde(sc1.getStunde().getId())) {
					Toast.makeText(getActivity(), getString(R.string.context_menu_stundelist_stunde_geloescht), Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(getActivity(), getString(R.string.fehler_beim_loeschen), Toast.LENGTH_LONG).show();
				}
				updateUI();
				mode.finish();
				return true;
			case R.id.context_menu_deleteVeranstaltung:
				StundeCursor sc2 = (StundeCursor) getAdapter().getItem(mSelectedItem);

				// TODO: Undo m�glichkeit?

				if (mDataModel.deleteVeranstaltung(sc2.getStunde().getVeranstaltungId())) {
					Toast.makeText(getActivity(), getString(R.string.context_menu_stundelist_veranstaltung_geloescht), Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(getActivity(), getString(R.string.fehler_beim_loeschen), Toast.LENGTH_LONG).show();
				}
				updateUI();
				mode.finish();
				return true;
			case R.id.context_menu_showVeranstaltung:
				StundeCursor sc3 = (StundeCursor) getAdapter().getItem(mSelectedItem);

				Intent i = new Intent(getActivity(), VeranstaltungActivity.class);
				i.putExtra(VeranstaltungActivity.EXTRA_VERANSTALTUNG_ID, sc3.getStunde().getVeranstaltungId());
				getActivity().startActivityForResult(i, StudOrgaActivity.REQUEST_VERANSTALTUNG);

				mode.finish();
				return true;
			default:
				mode.finish();
				return false;
			}

		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}
	};

	public StundenplanTabFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_stundenplan, container, false);
		updateSemester();

		TabHost tabHost = (TabHost) v.findViewById(R.id.tabHost);
		tabHost.setup();



		String[] tage = getResources().getStringArray(R.array.wochentage_abk);
		
		TabSpec s1 = tabHost.newTabSpec("Tab1");
		TabSpec s2 = tabHost.newTabSpec("Tab2");
		TabSpec s3 = tabHost.newTabSpec("Tab3");
		TabSpec s4 = tabHost.newTabSpec("Tab4");
		TabSpec s5 = tabHost.newTabSpec("Tab5");
		
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
//			tabHost.getTabWidget().setDividerDrawable(com.actionbarsherlock.R.drawable.abs__list_divider_holo_light); TODO: FUER ACTIONBARCOMPAT
			tabHost.getTabWidget().setDividerDrawable(android.R.drawable.divider_horizontal_bright);

			s1.setIndicator(customTabTextView(tage[0]));
			s2.setIndicator(customTabTextView(tage[1]));
			s3.setIndicator(customTabTextView(tage[2]));
			s4.setIndicator(customTabTextView(tage[3]));
			s5.setIndicator(customTabTextView(tage[4]));
		} else {
			s1.setIndicator(tage[0]);
			s2.setIndicator(tage[1]);
			s3.setIndicator(tage[2]);
			s4.setIndicator(tage[3]);
			s5.setIndicator(tage[4]);
		}

		// ===== Montag Tab =====
		mMontagsCursor = mDataModel.getStundenAnTagInSemester(0, SELECTED_SEMESTER);

		s1.setContent(R.id.tab1);
		
		mListViewMontag = (ListView) v.findViewById(R.id.listview1);
		StundeCursorAdapter adapter = new StundeCursorAdapter(getActivity(), mMontagsCursor);
		mListViewMontag.setAdapter(adapter);

		mListViewMontag.setOnItemClickListener(new StundenOnItemClickListener(adapter));
		mListViewMontag.setOnItemLongClickListener(new LongClicker(adapter));

		// ===== Dienstag Tab =====
		mDienstagsCursor = mDataModel.getStundenAnTagInSemester(1, SELECTED_SEMESTER);

		s2.setContent(R.id.tab2);
		
		mListViewDienstag = (ListView) v.findViewById(R.id.listview2);
		StundeCursorAdapter adapter2 = new StundeCursorAdapter(getActivity(), mDienstagsCursor);
		mListViewDienstag.setAdapter(adapter2);
		mListViewDienstag.setOnItemClickListener(new StundenOnItemClickListener(adapter2));
		mListViewDienstag.setOnItemLongClickListener(new LongClicker(adapter2));

		// ===== Mittwoch Tab =====
		mMittwochsCursor = mDataModel.getStundenAnTagInSemester(2, SELECTED_SEMESTER);

		s3.setContent(R.id.tab3);

		mListViewMittwoch = (ListView) v.findViewById(R.id.listview3);
		StundeCursorAdapter adapter3 = new StundeCursorAdapter(getActivity(), mMittwochsCursor);
		mListViewMittwoch.setAdapter(adapter3);
		mListViewMittwoch.setOnItemClickListener(new StundenOnItemClickListener(adapter3));
		mListViewMittwoch.setOnItemLongClickListener(new LongClicker(adapter3));

		// ===== Donnerstag Tab =====
		mDonnerstagsCursor = mDataModel.getStundenAnTagInSemester(3, SELECTED_SEMESTER);

		s4.setContent(R.id.tab4);

		mListViewDonnerstag = (ListView) v.findViewById(R.id.listview4);
		StundeCursorAdapter adapter4 = new StundeCursorAdapter(getActivity(), mDonnerstagsCursor);
		mListViewDonnerstag.setAdapter(adapter4);
		mListViewDonnerstag.setOnItemClickListener(new StundenOnItemClickListener(adapter4));
		mListViewDonnerstag.setOnItemLongClickListener(new LongClicker(adapter4));

		// ===== Freitag Tab =====
		mFreitagsCursor = mDataModel.getStundenAnTagInSemester(4, SELECTED_SEMESTER);
		
		s5.setContent(R.id.tab5);

		mListViewFreitag = (ListView) v.findViewById(R.id.listview5);
		StundeCursorAdapter adapter5 = new StundeCursorAdapter(getActivity(), mFreitagsCursor);
		mListViewFreitag.setAdapter(adapter5);
		mListViewFreitag.setOnItemClickListener(new StundenOnItemClickListener(adapter5));
		mListViewFreitag.setOnItemLongClickListener(new LongClicker(adapter5));

		
		tabHost.addTab(s1);
		tabHost.addTab(s2);
		tabHost.addTab(s3);
		tabHost.addTab(s4);
		tabHost.addTab(s5);
		
		tabHost.setOnTabChangedListener(new OnTabChangeListener() {

			@Override
			public void onTabChanged(String tabId) {
				if (mAction != null) {
					mAction.finish();
					mAction = null;
				}

			}
		});

		// TODO: Machen, dass beim wechsel Port->Land nicht Tab gewechselt wird
		// int dow = FunkLib.getDayOfWeek();
		/*
		 * W�hlt Tab des aktuellen Tages aus. 2 = Montag -> Tab0, 6 = Freitag ->
		 * Tab4
		 */
		// if (dow >= 2 && dow <= 6) {
		// tabHost.setCurrentTab(dow - 2);
		// } else { // Samstags und Sonntags wird Montag angezeigt
		// tabHost.setCurrentTab(1);
		// }

		
	    if(Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB){
	        setTabsBackground(tabHost);
	    }

		return v;
	}
	
	private View customTabTextView(String text){
	    TextView txtTab = new TextView(getActivity());
	    txtTab.setText(text.toUpperCase(Locale.getDefault()));
	    txtTab.setPadding(0, 5, 0, 0);
	    txtTab.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
	    txtTab.setTextColor(Color.DKGRAY);
	    txtTab.setGravity(Gravity.CENTER);
	    txtTab.setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
	    txtTab.setHeight(50);

	    return txtTab;
	}
	
	private void setTabsBackground(TabHost tabHost) {
	    View v;
	    int count = tabHost.getTabWidget().getTabCount();
	    for (int i = 0; i < count; i++) {
	        v = tabHost.getTabWidget().getChildTabViewAt(i);
	        v.setBackgroundResource(R.drawable.tab_indicator_compat_holo);

	        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
	        params.setMargins(0, 0, 0, 0);
	    }
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.fragment_menu_stundenplan, menu);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		// setRetainInstance(true);
		mDataModel = UniDataModelFactory.createInstance(getActivity());
		// updateSemester();
	}

	public void updateSemester() {
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
		SELECTED_SEMESTER = Long.parseLong(settings.getString("selectedSemester", "" + mDataModel.getLatestSemesterId()));

		if (SELECTED_SEMESTER == -1) {
			SELECTED_SEMESTER = 0;
			Log.i("test", "SELECTED_SEMESTER Neu: " + mDataModel.getSemester(SELECTED_SEMESTER));
		}

	}

	/*
	 * onOptionsItemSelected wird automatisch aufgerufen, wenn ein Menue Item
	 * gedrueckt wird. Hier muss unterschieden werden (im switch) welches Menu
	 * gedr�ckt wurde und danach die gewuenschte Option aufgerufen werden
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
		long sid = Long.parseLong(settings.getString("selectedSemester", "" + mDataModel.getLatestSemesterId()));

		switch (item.getItemId()) {
		case R.id.submenu_item_new_stunde:

			if (mDataModel.getAlleVeranstaltungenInSemester(sid).getCount() < 1) {
				Toast.makeText(getActivity(), getString(R.string.StundenTabFragment_fehler_keine_veranstaltungen), Toast.LENGTH_LONG).show();
				return true;
			}

			getActivity().startActivityForResult(new Intent(getActivity(), StundeActivity.class), StudOrgaActivity.REQUEST_STUNDE);

			return true;
		case R.id.submenu_item_new_veranstaltung:

			if (mDataModel.getAlleSemester().getCount() < 1) {
				Toast.makeText(getActivity(), getString(R.string.StundenTabFragment_fehler_kein_semester), Toast.LENGTH_LONG).show();
				return true;
			}
			getActivity().startActivityForResult(new Intent(getActivity(), VeranstaltungActivity.class), StudOrgaActivity.REQUEST_VERANSTALTUNG);
			return true;
		case R.id.submenu_item_new_semester:
			getActivity().startActivityForResult(new Intent(getActivity(), SemesterActivity.class), StudOrgaActivity.UPDATED_SELECTED_SEMESTER);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public void updateUI() {
		mMontagsCursor.requery();
		((StundeCursorAdapter) mListViewMontag.getAdapter()).notifyDataSetChanged();
		mDienstagsCursor.requery();
		((StundeCursorAdapter) mListViewDienstag.getAdapter()).notifyDataSetChanged();
		mMittwochsCursor.requery();
		((StundeCursorAdapter) mListViewMittwoch.getAdapter()).notifyDataSetChanged();
		mDonnerstagsCursor.requery();
		((StundeCursorAdapter) mListViewDonnerstag.getAdapter()).notifyDataSetChanged();
		mFreitagsCursor.requery();
		((StundeCursorAdapter) mListViewFreitag.getAdapter()).notifyDataSetChanged();
	}

	/*
	 * Ein normaler Listener, der spaeter die Detailansicht einer Stunde oeffnen
	 * soll, wenn man auf einen Listeneintrag einer Stunde drueckt
	 */
	private class StundenOnItemClickListener implements OnItemClickListener {
		private StundeCursorAdapter mAdapter;

		public StundenOnItemClickListener(StundeCursorAdapter adapter) {
			this.mAdapter = adapter;
		}

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

			if (mAction == null) {
				Stunde s = ((StundeCursor) parent.getItemAtPosition(position)).getStunde();

				Intent i = new Intent(getActivity(), StundeActivity.class);
				i.putExtra(StundeActivity.EXTRA_STUNDE_ID, s.getId());

				getActivity().startActivityForResult(i, StudOrgaActivity.REQUEST_STUNDE);
			} else {
				// TerminCursorAdapter a = (TerminCursorAdapter)
				// getListAdapter();
				mAdapter.removeSelection();
				// a.removeSelection();
				mSelectedItem = position;
				// a.toggleSelection(mSelectedItem);
				mAdapter.toggleSelection(mSelectedItem);
			}

		}
	}

	private class LongClicker implements OnItemLongClickListener {
		private StundeCursorAdapter mAdapter;

		public LongClicker(StundeCursorAdapter a) {
			this.mAdapter = a;
		}

		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
			if (mAction != null)
				return false;

			mSelectedItem = position;

			mAdapter.toggleSelection(mSelectedItem);

			mAction = ((ActionBarActivity)getActivity()).startSupportActionMode(new MyActionThing(mAdapter));
			return true;
		}

	}

	public static class StundeCursorAdapter extends CursorAdapter {

		private StundeCursor mStundeCursor;
		private SparseBooleanArray mSelectedItems;

		public StundeCursorAdapter(Context context, StundeCursor c) {
			super(context, c, 0);
			this.mStundeCursor = c;
			mSelectedItems = new SparseBooleanArray();
		}

		public void toggleSelection(int position) {
			selectView(position, !mSelectedItems.get(position));
		}

		public void removeSelection() {
			mSelectedItems = new SparseBooleanArray();
			notifyDataSetChanged();
		}

		public void selectView(int position, boolean value) {
			if (value)
				mSelectedItems.put(position, value);
			else
				mSelectedItems.delete(position);

			notifyDataSetChanged();
		}

		public int getSelectedCount() {
			return mSelectedItems.size();
		}

		public SparseBooleanArray getSelectedIds() {
			return mSelectedItems;
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			return inflater.inflate(R.layout.list_item_stunde, parent, false); // simple_list_item_1
		}

		@Override
		public void bindView(View v, Context c, Cursor cursor) {
			Stunde s = mStundeCursor.getStunde();

			TextView tv_uhrzeit = (TextView) v.findViewById(R.id.list_textView_Uhrzeit);
			TextView tv_raum = (TextView) v.findViewById(R.id.list_textView_Raum);
			TextView tv_art = (TextView) v.findViewById(R.id.list_textView_Art);
			TextView tv_name = (TextView) v.findViewById(R.id.list_textView_Name);

			tv_uhrzeit.setText(s.getTime());
			tv_raum.setText(s.getRaum());
			tv_art.setText(s.getArt());

			IUniDataModel model = UniDataModelFactory.createInstance(mContext);

			tv_name.setText(model.getVeranstaltung(s.getVeranstaltungId()).getName());

			v.setBackgroundColor(mSelectedItems.get(cursor.getPosition()) ? 0x9934B5E4 : Color.TRANSPARENT);

		}
	}
}