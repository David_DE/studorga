package de.frost.david.android.studorga;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;
import de.frost.david.android.studorga.R;
import de.frost.david.android.studorga.lib.FunkLib;
import de.frost.david.android.studorga.model.IUniDataModel;
import de.frost.david.android.studorga.model.Termin;
import de.frost.david.android.studorga.model.UniDataModelFactory;

public class TerminFragment extends Fragment {
	
	private EditText mTerminNameEditText;
	private EditText mTerminOrtEditText;
	private TimePickerDialog mUhrzeitDialog;
	private DatePickerDialog mDateDialog;
	private Button mSpeichernButton;
	private Button mUhrzeitButton;
	private Button mDatumButton;
	private int mHour, mMinute, mDay, mMonth, mYear;
	private boolean isLandscape;
	
	
	public static final String NEW_TERMIN = "de.thm.swtp.android.studorga.NEW_TERMIN";
	private static final String ARG_TERMIN_ID = "TERMIN_ID";
	
	private IUniDataModel mDataModel;
	private Termin mTermin;

	public static Fragment newInstance(long tid) {
		Bundle args = new Bundle();
		args.putLong(ARG_TERMIN_ID, tid);
		TerminFragment tf = new TerminFragment();
		tf.setArguments(args);
		return tf;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		mDataModel = UniDataModelFactory.createInstance(getActivity());

		Bundle args = getArguments();
		if (args != null) {
			long id = args.getLong(ARG_TERMIN_ID, -1);
			if (id != -1) {
				mTermin = mDataModel.getTermin(id);
			}
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		((ActionBarActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
		((ActionBarActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		getActivity().setTitle(getString(R.string.add_new_termin));
		View v = inflater.inflate(R.layout.fragment_form_termine, container, false);

		setHasOptionsMenu(true);
		
		mSpeichernButton = (Button)v.findViewById(R.id.termineFormular_speichern_Button);
		mSpeichernButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				setTerminResultAndGo();
				
			}
		});
		
		mTerminNameEditText = (EditText)v.findViewById(R.id.termineFormular_name_EditText);
		mTerminOrtEditText = (EditText)v.findViewById(R.id.termineFormular_ort_EditText);
		
		if (mTermin != null) {
			mTerminNameEditText.setText(mTermin.getName());
			mTerminOrtEditText.setText(mTermin.getOrt());
			
			mMinute = mTermin.getMinute();
			mHour = mTermin.getHour();
			mDay = mTermin.getDay();
			mMonth = mTermin.getMonth();
			mYear = mTermin.getYear();
			
		} else {
			mMinute = FunkLib.getMinute();
			mHour = FunkLib.getHour();
			mDay = FunkLib.getDay();
			mMonth = FunkLib.getMonth();
			mYear = FunkLib.getYear();
		}
		
		
		mUhrzeitDialog = new TimePickerDialog(getActivity(), new TimePickHandler(), mHour, mMinute, true);
		mDateDialog = new DatePickerDialog(getActivity(), new DatePickHandler(), mYear, mMonth-1, mDay);
		
		isLandscape = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
		
		String maybeneline = (!isLandscape)? "\n" : " ";
		
		mUhrzeitButton = (Button)v.findViewById(R.id.termineFormular_Uhrzeit_Button);
		mUhrzeitButton.setText(getText(R.string.TerminFragment_uhrzeit) + maybeneline + "("+FunkLib.convert(mHour)+":"+FunkLib.convert(mMinute)+")");
		mUhrzeitButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mUhrzeitDialog.show();
			}
		});
		
		mDatumButton = (Button)v.findViewById(R.id.termineFormular_Datum_Button);
		mDatumButton.setText(getText(R.string.TerminFragment_datum) + maybeneline + "("+FunkLib.convert(mDay)+"."+FunkLib.convert(mMonth)+"."+mYear+")");
		mDatumButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mDateDialog.show();
			}
		});
		
		return v;
	}
	
	/*
	 * setTerminResult ueberprueft, ob alle Felder Eingaben beinhalten und speicher diese in ein Termin Objekt, 
	 * welches in einem Intent der Acitivt�t "angehaengt" wird	
	 */
	private void setTerminResultAndGo() {
		String name = mTerminNameEditText.getText().toString();
		String ort = mTerminOrtEditText.getText().toString();
		
		if (name.equals("")) {
			Toast.makeText(getActivity(), getString(R.string.TerminFragment_fehler_name), Toast.LENGTH_SHORT).show();
			return;
		} else if (ort.equals("")) {
			Toast.makeText(getActivity(), getString(R.string.TerminFragment_fehler_ort), Toast.LENGTH_SHORT).show();
			return;
		}
		
		if (mTermin != null) {
			mTermin.setName(name);
			mTermin.setOrt(ort);
			mTermin.setDay(mDay);
			mTermin.setMonth(mMonth);
			mTermin.setYear(mYear);
			mTermin.setHour(mHour);
			mTermin.setMinute(mMinute);
			mDataModel.updateTermin(mTermin);
		} else {
			mTermin = new Termin(name, ort, mDay, mMonth, mYear, mHour, mMinute);
			mTermin.setVeranstaltungsId(-1);
			mTermin.setId(mDataModel.insertTermin(mTermin));
		}
		
		//TODO: Data braucht nicht zur�ckgegeben werden
		Intent data = new Intent();
//		data.putExtra(NEW_TERMIN, new Termin(name, ort, mDay, mMonth, mYear, mHour, mMinute));
		getActivity().setResult(ActionBarActivity.RESULT_OK, data);
		getActivity().finish();
		
	}
	
	class TimePickHandler implements OnTimeSetListener {
		
		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			mHour = hourOfDay;
			mMinute = minute;
			String maybeneline = (isLandscape)? "\n" : " ";
			mUhrzeitButton.setText(getText(R.string.TerminFragment_uhrzeit) + maybeneline +"("+FunkLib.convert(mHour)+":"+FunkLib.convert(mMinute)+")");
			mUhrzeitDialog.hide();
		}
	}
	
	class DatePickHandler implements OnDateSetListener {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear+1;
			mDay = dayOfMonth;
			String maybeneline = (isLandscape)? "\n" : " ";
			mDatumButton.setText(getText(R.string.TerminFragment_datum) + maybeneline +"("+FunkLib.convert(mDay)+"."+FunkLib.convert(mMonth)+"."+mYear+")");
			mDateDialog.hide();
		}
		
	}



}
