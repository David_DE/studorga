package de.frost.david.android.studorga;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import de.frost.david.android.studorga.R;
import de.frost.david.android.studorga.lib.FunkLib;
import de.frost.david.android.studorga.model.IUniDataModel;
import de.frost.david.android.studorga.model.Semester;
import de.frost.david.android.studorga.model.UniDataModelFactory;

public class SemesterFragment extends Fragment {

	private EditText mSemesterNameEditText;
	private CheckBox mAuswaehlenCheckBox;
	private Button mSpeichernButton;
	

	public static final String NEW_SEMESTER = "de.thm.swtp.android.studorga.NEW_SEMESTER";
	private static final String ARG_SEMESTER_ID = "SEMESTER_ID";
	
	private IUniDataModel mDataModel;
	private Semester mSemester;
	
	public static Fragment newInstance(long sid) {
		Bundle args = new Bundle();
		args.putLong(ARG_SEMESTER_ID, sid);
		SemesterFragment vf = new SemesterFragment();
		vf.setArguments(args);
		return vf;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		mDataModel = UniDataModelFactory.createInstance(getActivity());

		Bundle args = getArguments();
		if (args != null) {
			long id = args.getLong(ARG_SEMESTER_ID, -1);
			if (id != -1) {
				mSemester = mDataModel.getSemester(id);
			}
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

//		getActivity().getSupportActionBar().setHomeButtonEnabled(true);
//		getActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		getActivity().setTitle(getString(R.string.add_new_semester));
		View v = inflater.inflate(R.layout.fragment_form_semester, container, false);

		setHasOptionsMenu(true);

		mSpeichernButton = (Button) v.findViewById(R.id.semesterFormular_speichern_Button);
		mSpeichernButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				setSemesterResultAndGo();

			}
		});
		
		mSemesterNameEditText = (EditText) v.findViewById(R.id.semesterFormular_name_EditText);
		mAuswaehlenCheckBox = (CheckBox) v.findViewById(R.id.semesterFormular_auswaehlen_CheckBox);
		
		mAuswaehlenCheckBox.setChecked(true);

		if (mDataModel.getAlleSemester().getCount() == 0) {
			mAuswaehlenCheckBox.setEnabled(false);
		}

		if (mSemester != null) {
			mSemesterNameEditText.setText(mSemester.getName());
			
		} else {
			mSemesterNameEditText.setText(FunkLib.getSemesterName());
		}
		
		return v;
	}

	private void setSemesterResultAndGo() {
		String name = mSemesterNameEditText.getText().toString();

		if (name.equals("")) {
			Toast.makeText(getActivity(), getString(R.string.VeranstaltungFragment_fehler_name), Toast.LENGTH_SHORT).show();
			return;
		} 
		

		
		if (mSemester != null) {
			mSemester.setName(name);
			mDataModel.updateSemester(mSemester);
		} else {
			mSemester = new Semester(name);
			mSemester.setId(mDataModel.insertSemester(mSemester));
		}
		
		if (mAuswaehlenCheckBox.isChecked()) {
			SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
			SharedPreferences.Editor editor = settings.edit();
			editor.putString("selectedSemester", ""+mSemester.getId());
			editor.commit();
			getActivity().setResult(ActionBarActivity.RESULT_OK, new Intent());
		}
		
		getActivity().finish();

	}
}
