package de.frost.david.android.studorga.model;

import de.frost.david.android.studorga.lib.FunkLib;


public class Stunde {
	
	private long mId;
	private long mVeranstaltungId;
	private int mWochentag;
	private String mArt; // Uebung, Vorlesung, Labor, etc
	private String mRaum;
	private String mDozent;
	private int mStart_stunde, mStart_minute, mEnd_stunde, mEnd_minute;
	
	public Stunde(String art, String raum, int tag, String dozent, int start_stunde, int start_minute, int end_stunde, int end_minute) {
		mArt = art;
		mRaum = raum;
		mDozent = dozent;
		this.mStart_minute = start_minute;
		this.mStart_stunde = start_stunde;
		this.mEnd_minute = end_minute;
		this.mEnd_stunde = end_stunde;
		
		this.mId = -1;
		this.mVeranstaltungId = -1;
		this.mWochentag = tag;
	}
	
	public String getTime() {
		String temp = "";
		
		temp += FunkLib.convert(mStart_stunde);
		temp += ":";
		temp += FunkLib.convert(mStart_minute);
		temp += " - ";
		temp += FunkLib.convert(mEnd_stunde);
		temp += ":";
		temp += FunkLib.convert(mEnd_minute);
		
		return temp;
	}

	public String getArt() {
		return mArt;
	}

	public void setArt(String art) {
		mArt = art;
	}

	public String getRaum() {
		return mRaum;
	}

	public void setRaum(String raum) {
		mRaum = raum;
	}

	public String getDozent() {
		return mDozent;
	}

	public void setDozent(String dozent) {
		mDozent = dozent;
	}

	public int getStart_stunde() {
		return mStart_stunde;
	}

	public void setStart_stunde(int start_stunde) {
		this.mStart_stunde = start_stunde;
	}

	public int getStart_minute() {
		return mStart_minute;
	}

	public void setStart_minute(int start_minute) {
		this.mStart_minute = start_minute;
	}

	public int getEnd_stunde() {
		return mEnd_stunde;
	}

	public void setEnd_stunde(int end_stunde) {
		this.mEnd_stunde = end_stunde;
	}

	public int getEnd_minute() {
		return mEnd_minute;
	}

	public void setEnd_minute(int end_minute) {
		this.mEnd_minute = end_minute;
	}

	public long getId() {
		return this.mId;
	}
	
	public void setId(long id) {
		this.mId = id;
	}

	public long getVeranstaltungId() {
		return this.mVeranstaltungId;
	}
	
	public void setVeranstaltungId(long veranstaltung_id) {
		this.mVeranstaltungId = veranstaltung_id;
	}

	public int getWochentag() {
		return mWochentag;
	}

	public void setWochentag(int wochentag) {
		mWochentag = wochentag;
	}


}
