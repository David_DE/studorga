package de.frost.david.android.studorga.model;

import android.content.Context;

public class UniDataModelFactory {
	private static IUniDataModel mUniDataModell = null;
	
	public static IUniDataModel createInstance(Context c) {
		if (mUniDataModell == null) {
			mUniDataModell = new UniDatabaseHelper(c);
		}
		return mUniDataModell;
	}

}
