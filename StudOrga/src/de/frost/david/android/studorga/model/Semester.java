package de.frost.david.android.studorga.model;

public class Semester {
	private String mName;
	private long mId;
	
	public Semester(String name) {
		this.mName = name;
		this.mId = -1;
	}

	public String getName() {
		return mName;
	}

	public long getId() {
		return mId;
	}

	public void setName(String name) {
		mName = name;
	}

	public void setId(long id) {
		mId = id;
	}
}
