package de.frost.david.android.studorga.model;

import de.frost.david.android.studorga.model.UniDatabaseHelper.SemesterCursor;
import de.frost.david.android.studorga.model.UniDatabaseHelper.StundeCursor;
import de.frost.david.android.studorga.model.UniDatabaseHelper.TerminCursor;
import de.frost.david.android.studorga.model.UniDatabaseHelper.VeranstaltungCursor;

public interface IUniDataModel {

	//STUNDE
	StundeCursor getStundenAnTagInSemester(long tag, long semester);
	
	long insertStunde(Stunde s);
	
	Stunde getStunde(long id);
	
	void updateStunde(Stunde s);
	
	boolean deleteStunde(long id);
	
	//TERMINE	
	TerminCursor getAlleTermine();
	
	TerminCursor getAlleTermineAktuell();

	long insertTermin(Termin t);

	Termin getTermin(long id);

	void updateTermin(Termin termin);
	
	boolean deleteTermin(long id);
	
	//VERANSTALTUNGEN
	long insertVeranstaltung(Veranstaltung v);

	Veranstaltung getVeranstaltung(long id);
	
	VeranstaltungCursor getAlleVeranstaltungen();
	
	void updateVeranstaltung(Veranstaltung v);
	
	boolean deleteVeranstaltung(long id);
	
	VeranstaltungCursor getAlleVeranstaltungenInSemester(long sid);
	
	//SEMESTER

	SemesterCursor getAlleSemester();

	Semester getSemester(long id);

	long insertSemester(Semester s);

	void updateSemester(Semester s);

	long getLatestSemesterId();

}
