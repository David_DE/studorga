package de.frost.david.android.studorga.model;

public class Veranstaltung {

	private long mId;
	private long mSemesterId;
	private String mName;
	private String mAbkuerzung;
	private int mCredits;
	
	public Veranstaltung(String name, String abk) {
		this.mName = name;
		this.mAbkuerzung = abk;
		this.mCredits = 0;
		this.mId = -1;
		this.mSemesterId = -1;
	}

	public String getName() {
		return mName;
	}

	public void setName(String name) {
		mName = name;
	}

	public String getAbkuerzung() {
		return mAbkuerzung;
	}

	public void setAbkuerzung(String abkuerzung) {
		mAbkuerzung = abkuerzung;
	}

	public int getCredits() {
		return mCredits;
	}

	public void setCredits(int credits) {
		mCredits = credits;
	}
	
	public void setId(long id) {
		this.mId = id;
	}
	
	public long getId() {
		return this.mId;
	}

	public void setSemesterId(long sid) {
		this.mSemesterId = sid;
	}
	
	public long getSemesterId() {
		return this.mSemesterId;
	}
	
	@Override
	public String toString() {
		return this.mName;
	}



}
