package de.frost.david.android.studorga.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

import de.frost.david.android.studorga.lib.FunkLib;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

public class UniDatabaseHelper extends SQLiteOpenHelper implements IUniDataModel {
	private static final String DB_NAME = "uni.sqlite";
	private static final int VERSION = 1;
	
	private static final String TABLE_SEMESTER = "semester";
	private static final String COLUMN_SEMESTER_NAME = "name";

	private static final String TABLE_VERANSTALTUNG = "veranstaltung";
	private static final String COLUMN_VERANSTALTUNG_NAME = "name";
	private static final String COLUMN_VERANSTALTUNG_ABK = "abk";
	private static final String COLUMN_VERANSTALTUNG_CREDITS = "credits";
	private static final String COLUMN_VERANSTALTUNG_SEMESER_ID = "semester_id";

	private static final String TABLE_STUNDE = "stunde";
	private static final String COLUMN_STUNDE_ART = "art";
	private static final String COLUMN_STUNDE_RAUM = "raum";
	private static final String COLUMN_STUNDE_DOZENT = "dozent";
	private static final String COLUMN_STUNDE_TAG = "tag";
	private static final String COLUMN_STUNDE_START_STUNDE = "start_stunde";
	private static final String COLUMN_STUNDE_START_MINUTE = "start_minute";
	private static final String COLUMN_STUNDE_END_STUNDE = "end_stunde";
	private static final String COLUMN_STUNDE_END_MINUTE = "end_minute";
	private static final String COLUMN_STUNDE_VERANSTALTUNG_ID = "veranstaltung_id";

	private static final String TABLE_TERMIN = "wochentag";
	private static final String COLUMN_TERMIN_NAME = "name";
	private static final String COLUMN_TERMIN_ORT = "ort";
	private static final String COLUMN_TERMIN_TAG = "tag";
	private static final String COLUMN_TERMIN_MONAT = "monat";
	private static final String COLUMN_TERMIN_JAHR = "jahr";
	private static final String COLUMN_TERMIN_STUNDE = "stunde";
	private static final String COLUMN_TERMIN_MINUTE = "minute";
	private static final String COLUMN_TERMIN_VERANSTALTUNG_ID = "veranstaltung_id";

	private static final String TABLE_AUFGABE = "aufgabe";
	private static final String COLUMN_AUFGABE_NAME = "name";
	private static final String COLUMN_AUFGABE_BESCHREIBUNG = "beschreibung";
	private static final String COLUMN_AUFGABE_ABGESCHLOSSEN = "abgeschlossen";
	private static final String COLUMN_AUFGABE_DEADLINE = "deadline";
	private static final String COLUMN_AUFGABE_VERANSTALTUNG_ID = "veranstaltung_id";

	public UniDatabaseHelper(Context context) {
		super(context, DB_NAME, null, VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		
		Log.i("test", "in onCreate -> SQLite Helper");
		
		db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_SEMESTER + " (_id integer primary key autoincrement, " 
				+ 	COLUMN_SEMESTER_NAME + " varchar(100))");

		db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_VERANSTALTUNG + " (_id integer primary key autoincrement, " 
				+ COLUMN_VERANSTALTUNG_NAME + " varchar(100), " 
				+ COLUMN_VERANSTALTUNG_ABK + " varchar(5), " 
				+ COLUMN_VERANSTALTUNG_CREDITS + " integer, "
				+ COLUMN_VERANSTALTUNG_SEMESER_ID + " integer references " + TABLE_SEMESTER + "(_id))");

		db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_STUNDE + " (_id integer primary key autoincrement, " 
				+ COLUMN_STUNDE_ART + " varchar(15), " 
				+ COLUMN_STUNDE_RAUM + " varchar(10), " 
				+ COLUMN_STUNDE_DOZENT + " varchar(100), " 
				+ COLUMN_STUNDE_TAG + " integer, "
				+ COLUMN_STUNDE_START_STUNDE + " integer, " 
				+ COLUMN_STUNDE_START_MINUTE +" integer, "
				+ COLUMN_STUNDE_END_STUNDE + " integer, " 
				+ COLUMN_STUNDE_END_MINUTE + " integer, " 
				+ COLUMN_STUNDE_VERANSTALTUNG_ID + " integer references " +  TABLE_VERANSTALTUNG + "(_id))");

		db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_TERMIN + " (_id integer primary key autoincrement, " 
				+ COLUMN_TERMIN_NAME + " varchar(100), " 
				+ COLUMN_TERMIN_ORT + " varchar(100), " 
				+ COLUMN_TERMIN_TAG + " integer, " 
				+ COLUMN_TERMIN_MONAT + " integer, " 
				+ COLUMN_TERMIN_JAHR + " integer, " 
				+ COLUMN_TERMIN_STUNDE + " integer, " 
				+ COLUMN_TERMIN_MINUTE + " integer, " 
				+ COLUMN_TERMIN_VERANSTALTUNG_ID + " integer references " +  TABLE_VERANSTALTUNG + "(_id))");		

		db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_AUFGABE + " (_id integer primary key autoincrement, " 
				+ COLUMN_AUFGABE_NAME + " varchar(100), " 
				+ COLUMN_AUFGABE_BESCHREIBUNG + " varchar(250), " 
				+ COLUMN_AUFGABE_ABGESCHLOSSEN + " integer, " 
				+ COLUMN_AUFGABE_DEADLINE + " varchar(100), " 
				+ COLUMN_AUFGABE_VERANSTALTUNG_ID + " integer references " +  TABLE_VERANSTALTUNG + "(_id))");		
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}
	
	public void dropTables() {
		getWritableDatabase().rawQuery("DROP TABLE " + TABLE_STUNDE, null);
		getWritableDatabase().rawQuery("DROP TABLE " + TABLE_VERANSTALTUNG, null);
		
		this.onCreate(getWritableDatabase());
	}
	
	public long insertSemester(Semester s) {
		ContentValues cv = new ContentValues();
		cv.put(COLUMN_SEMESTER_NAME, s.getName());
		
		return getWritableDatabase().insert(TABLE_SEMESTER, null, cv);
	}

	public long insertVeranstaltung(Veranstaltung v) {
		ContentValues cv = new ContentValues();
		cv.put(COLUMN_VERANSTALTUNG_NAME, v.getName());
		cv.put(COLUMN_VERANSTALTUNG_ABK, v.getAbkuerzung());
		cv.put(COLUMN_VERANSTALTUNG_CREDITS, v.getCredits());
		cv.put(COLUMN_VERANSTALTUNG_SEMESER_ID, v.getSemesterId());

		return getWritableDatabase().insert(TABLE_VERANSTALTUNG, null, cv);
	}

	public long insertStunde(Stunde s) {
		ContentValues cv = new ContentValues();
		cv.put(COLUMN_STUNDE_ART, s.getArt());
		cv.put(COLUMN_STUNDE_RAUM, s.getRaum());
		cv.put(COLUMN_STUNDE_DOZENT, s.getDozent());
		cv.put(COLUMN_STUNDE_TAG, s.getWochentag());
		cv.put(COLUMN_STUNDE_START_STUNDE, s.getStart_stunde());
		cv.put(COLUMN_STUNDE_START_MINUTE, s.getStart_minute());
		cv.put(COLUMN_STUNDE_END_STUNDE, s.getEnd_stunde());
		cv.put(COLUMN_STUNDE_END_MINUTE, s.getEnd_minute());
		cv.put(COLUMN_STUNDE_VERANSTALTUNG_ID, s.getVeranstaltungId());

		return getWritableDatabase().insert(TABLE_STUNDE, null, cv);
	}
	
	public long insertTermin(Termin t) {
		ContentValues cv = new ContentValues();

		cv.put(COLUMN_TERMIN_NAME, t.getName());
		cv.put(COLUMN_TERMIN_ORT, t.getOrt());
		cv.put(COLUMN_TERMIN_TAG, t.getDay());
		cv.put(COLUMN_TERMIN_MONAT, t.getMonth());
		cv.put(COLUMN_TERMIN_JAHR, t.getYear());
		cv.put(COLUMN_TERMIN_STUNDE, t.getHour());
		cv.put(COLUMN_TERMIN_MINUTE, t.getMinute());
		cv.put(COLUMN_TERMIN_VERANSTALTUNG_ID, t.getVeranstaltungsId());
		
		return getWritableDatabase().insert(TABLE_TERMIN, null, cv);
	}
	
	public SemesterCursor getAlleSemester() {
		Cursor wrapped = getReadableDatabase().query(TABLE_SEMESTER, null, null, null, null, null, null);
		return new SemesterCursor(wrapped);
	}
	
	public VeranstaltungCursor getAlleVeranstaltungen() {
		Cursor wrapped = getReadableDatabase().query(TABLE_VERANSTALTUNG, null, null, null, null, null, null);
		return new VeranstaltungCursor(wrapped);
	}

	public StundeCursor queryStunde() {
		Cursor wrapped = getReadableDatabase().query(TABLE_STUNDE, null, null, null, null, null, null);
		return new StundeCursor(wrapped);
	}
	
	//TODO: Sortieren nach Zeit. ggf. abgelaufene ausblenden?
	public TerminCursor getAlleTermine() {
		Cursor wrapped = getReadableDatabase().query(TABLE_TERMIN, null, null, null, null, null, "jahr, monat, tag, stunde, minute");
		return new TerminCursor(wrapped);
	}

	public TerminCursor getAlleTermineAktuell() {
		int year = FunkLib.getYear();
		int month = FunkLib.getMonth();
		int day = FunkLib.getDay();
		Cursor wrapped = getReadableDatabase().query(TABLE_TERMIN, null, "jahr >=? AND monat >=? AND tag >=?", new String[]{""+year, ""+month, ""+day}, null, null, "jahr, monat, tag, stunde, minute");
		return new TerminCursor(wrapped);
	}
	
	
	public Semester getSemester(long id) {
		Cursor wrapped = getReadableDatabase().query(TABLE_SEMESTER, null, "_id=?", new String[]{""+id}, null, null, null);
		SemesterCursor sc = new SemesterCursor(wrapped);
		
		sc.moveToFirst();		
		Semester s = sc.getSemester();
		sc.close();
		return s;
	}

	public Veranstaltung getVeranstaltung(long id) {
		Cursor wrapped = getReadableDatabase().query(TABLE_VERANSTALTUNG, null, "_id=?", new String[]{""+id}, null, null, null);
		VeranstaltungCursor vc = new VeranstaltungCursor(wrapped);
		
		vc.moveToFirst();		
		Veranstaltung v = vc.getVeranstaltung();
		vc.close();
		return v;
	}

	public Stunde getStunde(long id) {
		Cursor wrapped = getReadableDatabase().query(TABLE_STUNDE, null, "_id=?", new String[]{""+id}, null, null, null);
		StundeCursor sc = new StundeCursor(wrapped);
		
		sc.moveToFirst();		
		Stunde s = sc.getStunde();
		sc.close();
		return s;
	}

	public Termin getTermin(long id) {
		Cursor wrapped = getReadableDatabase().query(TABLE_TERMIN, null, "_id=?", new String[]{""+id}, null, null, null);
		TerminCursor tc = new TerminCursor(wrapped);
		
		tc.moveToFirst();		
		Termin t = tc.getTermin();
		tc.close();
		return t;
	}
	
	public void updateTermin(Termin t) {
		ContentValues cv = new ContentValues();
		
		cv.put(COLUMN_TERMIN_NAME, t.getName());
		cv.put(COLUMN_TERMIN_ORT, t.getOrt());
		cv.put(COLUMN_TERMIN_TAG, t.getDay());
		cv.put(COLUMN_TERMIN_MONAT, t.getMonth());
		cv.put(COLUMN_TERMIN_JAHR, t.getYear());
		cv.put(COLUMN_TERMIN_STUNDE, t.getHour());
		cv.put(COLUMN_TERMIN_MINUTE, t.getMinute());
		cv.put(COLUMN_TERMIN_VERANSTALTUNG_ID, t.getVeranstaltungsId());
		
		getWritableDatabase().update(TABLE_TERMIN, cv, "_id=?", new String[] {"" + t.getId()});
		
	}
	
	public void updateStunde(Stunde s) {
		ContentValues cv = new ContentValues();
		
		cv.put(COLUMN_STUNDE_ART, s.getArt());
		cv.put(COLUMN_STUNDE_RAUM, s.getRaum());
		cv.put(COLUMN_STUNDE_DOZENT, s.getDozent());
		cv.put(COLUMN_STUNDE_TAG, s.getWochentag());
		cv.put(COLUMN_STUNDE_START_STUNDE, s.getStart_stunde());
		cv.put(COLUMN_STUNDE_START_MINUTE, s.getStart_minute());
		cv.put(COLUMN_STUNDE_END_STUNDE, s.getEnd_stunde());
		cv.put(COLUMN_STUNDE_END_MINUTE, s.getEnd_minute());
		cv.put(COLUMN_STUNDE_VERANSTALTUNG_ID, s.getVeranstaltungId());

		getWritableDatabase().update(TABLE_STUNDE, cv, "_id=?", new String[] {"" + s.getId()});
	}

	public void updateVeranstaltung(Veranstaltung v) {
		ContentValues cv = new ContentValues();
		
		cv.put(COLUMN_VERANSTALTUNG_NAME, v.getName());
		cv.put(COLUMN_VERANSTALTUNG_ABK, v.getAbkuerzung());
		cv.put(COLUMN_VERANSTALTUNG_CREDITS, v.getCredits());
		cv.put(COLUMN_VERANSTALTUNG_SEMESER_ID, v.getSemesterId());
		
		getWritableDatabase().update(TABLE_VERANSTALTUNG, cv, "_id=?", new String[] {"" + v.getId()});
	}
	
	public void updateSemester(Semester s) {
		ContentValues cv = new ContentValues();
		
		cv.put(COLUMN_SEMESTER_NAME, s.getName());
		
		getWritableDatabase().update(TABLE_SEMESTER, cv, "_id=?", new String[] {"" + s.getId()});
	}
	
	public boolean deleteTermin(long id) {
		return (getWritableDatabase().delete(TABLE_TERMIN, "_id=?", new String[] {"" + id}) == 1);
	}
	
	public boolean deleteStunde(long id) {
		return (getWritableDatabase().delete(TABLE_STUNDE, "_id=?", new String[] {"" + id}) == 1);
	}
	
	public boolean deleteVeranstaltung(long id) {
		getWritableDatabase().delete(TABLE_STUNDE, COLUMN_STUNDE_VERANSTALTUNG_ID+"=?", new String[] {"" + id});
		return (getWritableDatabase().delete(TABLE_VERANSTALTUNG, "_id=?", new String[] {"" + id}) == 1);
	}

	public boolean deleteSemester(long id) {
		return (getWritableDatabase().delete(TABLE_SEMESTER, "_id=?", new String[] {"" + id}) == 1);
	}
	
	public StundeCursor getStundenAnTagInSemester(long tag, long semester) {
		String q_join = "SELECT stunde._id, art, raum, dozent, tag, start_stunde, start_minute, end_stunde, end_minute, veranstaltung_id, semester_id " +
				"FROM stunde " +
				"INNER JOIN veranstaltung ON stunde.veranstaltung_id = veranstaltung._id " +
				"WHERE tag = ? " +
				"AND semester_id = ? " +
				"ORDER BY start_stunde, start_minute, end_stunde, end_minute;";
		
		Cursor wrapped = getReadableDatabase().rawQuery(q_join, new String[]{""+tag, ""+semester});
		return new StundeCursor(wrapped);	
	}
	
	public VeranstaltungCursor getAlleVeranstaltungenInSemester(long sid) {
		Cursor wrapped = getReadableDatabase().query(TABLE_VERANSTALTUNG, null, "semester_id=?", new String[] {"" + sid}, null, null, null);
		return new VeranstaltungCursor(wrapped);
	}
	
	
	public long getLatestSemesterId() {
		Cursor wrapped = getReadableDatabase().query(TABLE_SEMESTER, null, null, null, null, null, "_id");
		SemesterCursor sc = new SemesterCursor(wrapped);
		
		if (sc.getCount() == 0) {
			sc.close();
			return -1;
		}
		
		sc.moveToFirst();		
		Semester s = sc.getSemester();
		sc.close();
		
		return s.getId();
	}
	
	
	
	
	//TODO: nur debug method
	@SuppressWarnings("resource")
	public void backup() {
		try {
	        File sd = Environment.getExternalStorageDirectory();
	        File data = Environment.getDataDirectory();

	        if (sd.canWrite()) {
	            String currentDBPath = "//data//de.thm.swtp.android.studorga//databases//uni.sqlite";
	            String backupDBPath = "uni.sqlite";
	            File currentDB = new File(data, currentDBPath);
	            File backupDB = new File(sd, backupDBPath);

	            if (currentDB.exists()) {
	                FileChannel src = new FileInputStream(currentDB).getChannel();
	                FileChannel dst = new FileOutputStream(backupDB).getChannel();
	                dst.transferFrom(src, 0, src.size());
	                src.close();
	                dst.close();
	            }
	        }
	    } catch (Exception e) {
	    }
	}

	public static class SemesterCursor extends CursorWrapper {
		
		public SemesterCursor(Cursor cursor) {
			super(cursor);
		}
		
		public Semester getSemester() {
			if (isBeforeFirst() || isAfterLast())
				return null;
			
			long id = getLong(getColumnIndex("_id"));
			String name = getString(getColumnIndex(COLUMN_SEMESTER_NAME));
			Semester s = new Semester(name);
			s.setId(id);
			
			return s;
		}
		
	}
	
	public static class VeranstaltungCursor extends CursorWrapper {

		public VeranstaltungCursor(Cursor cursor) {
			super(cursor);
		}

		public Veranstaltung getVeranstaltung() {
			if (isBeforeFirst() || isAfterLast())
				return null;

			long id = getLong(getColumnIndex("_id"));
			String name = getString(getColumnIndex(COLUMN_VERANSTALTUNG_NAME));
			String abk = getString(getColumnIndex(COLUMN_VERANSTALTUNG_ABK));
			Veranstaltung v = new Veranstaltung(name, abk);

			v.setCredits(getInt(getColumnIndex(COLUMN_VERANSTALTUNG_CREDITS)));
			v.setSemesterId(getLong(getColumnIndex(COLUMN_VERANSTALTUNG_SEMESER_ID)));
			v.setId(id);

			return v;
		}

	}

	public static class StundeCursor extends CursorWrapper {

		public StundeCursor(Cursor cursor) {
			super(cursor);
		}

		public Stunde getStunde() {
			if (isBeforeFirst() || isAfterLast())
				return null;

			long id = getLong(getColumnIndex("_id"));
			String art = getString(getColumnIndex(COLUMN_STUNDE_ART));
			String raum = getString(getColumnIndex(COLUMN_STUNDE_RAUM));
			String dozent = getString(getColumnIndex(COLUMN_STUNDE_DOZENT));
			int tag = getInt(getColumnIndex(COLUMN_STUNDE_TAG));
			int start_stunde = getInt(getColumnIndex(COLUMN_STUNDE_START_STUNDE));
			int start_minute = getInt(getColumnIndex(COLUMN_STUNDE_START_MINUTE));
			int end_stunde = getInt(getColumnIndex(COLUMN_STUNDE_END_STUNDE));
			int end_minute = getInt(getColumnIndex(COLUMN_STUNDE_END_MINUTE));
			long veranstaltung_id = getLong(getColumnIndex(COLUMN_STUNDE_VERANSTALTUNG_ID));

			Stunde s = new Stunde(art, raum, tag, dozent, start_stunde, start_minute, end_stunde, end_minute);
			s.setVeranstaltungId(veranstaltung_id);
			s.setId(id);

			return s;
		}

	}
	
	public static class TerminCursor extends CursorWrapper {
		
		public TerminCursor(Cursor cursor) {
			super(cursor);
		}
		
		public Termin getTermin() {
			if (isBeforeFirst() || isAfterLast())
				return null;
			
			long id = getLong(getColumnIndex("_id"));
			String name = getString(getColumnIndex(COLUMN_TERMIN_NAME));
			String ort = getString(getColumnIndex(COLUMN_TERMIN_ORT));
			int mDay = getInt(getColumnIndex(COLUMN_TERMIN_TAG));
			int mMonth = getInt(getColumnIndex(COLUMN_TERMIN_MONAT));
			int mYear = getInt(getColumnIndex(COLUMN_TERMIN_JAHR));
			int mHour = getInt(getColumnIndex(COLUMN_TERMIN_STUNDE));
			int mMinute = getInt(getColumnIndex(COLUMN_TERMIN_MINUTE));
			long veranstaltung_id = getLong(getColumnIndex(COLUMN_TERMIN_VERANSTALTUNG_ID));
			
			Termin t = new Termin(name, ort, mDay, mMonth, mYear, mHour, mMinute);
			t.setVeranstaltungsId(veranstaltung_id);
			t.setId(id);
			
			return t;
		}
		
	}
}
