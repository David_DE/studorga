package de.frost.david.android.studorga.model;

import de.frost.david.android.studorga.lib.FunkLib;

public class Termin {
	private String mName;
	private String mOrt;
	private int mDay, mMonth, mYear, mHour, mMinute;
	private long mId, mVeranstaltungsId;
	
	
	public Termin(String name, String ort, int mDay, int mMonth, int mYear, int mHour, int mMinute) {
		this.mName = name;
		this.mOrt = ort;
		this.mDay = mDay;
		this.mMonth = mMonth;
		this.mYear = mYear;
		this.mHour = mHour;
		this.mMinute = mMinute;
		this.mId = -1;
		this.mVeranstaltungsId = -1;
	}
	
	public String getTime() {
		String temp = "";
		
		temp += FunkLib.convert(mHour);
		temp += ":";
		temp += FunkLib.convert(mMinute);
		
		return temp;
	}
	
	public String getDate() {
		String temp = "";
		
		temp += FunkLib.convert(mDay);
		temp += ".";
		temp += FunkLib.convert(mMonth);
		temp += ".";
		temp += mYear;
		
		return temp;
	}
	
	public long getId() {
		return this.mId;
	}
	
	public void setId(long id) {
		this.mId = id;
	}

	public String getName() {
		return mName;
	}

	public void setName(String name) {
		mName = name;
	}

	public String getOrt() {
		return mOrt;
	}

	public void setOrt(String ort) {
		mOrt = ort;
	}

	public int getDay() {
		return mDay;
	}

	public int getMonth() {
		return mMonth;
	}

	public int getYear() {
		return mYear;
	}

	public void setDay(int day) {
		mDay = day;
	}

	public void setMonth(int month) {
		mMonth = month;
	}

	public void setYear(int year) {
		mYear = year;
	}

	public int getHour() {
		return mHour;
	}

	public int getMinute() {
		return mMinute;
	}

	public void setHour(int hour) {
		mHour = hour;
	}

	public void setMinute(int minute) {
		mMinute = minute;
	}

	public long getVeranstaltungsId() {
		return mVeranstaltungsId;
	}

	public void setVeranstaltungsId(long veranstaltungsId) {
		mVeranstaltungsId = veranstaltungsId;
	}
	



}
