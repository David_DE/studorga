package de.frost.david.android.studorga;

import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

public class SemesterActivity extends SingleFragmentActivity {

	public static final String EXTRA_SEMESTER_ID = "EXTRA_SEMESTER_ID";
	
	@Override
	protected Fragment createFragment() {
		long vid = getIntent().getLongExtra(EXTRA_SEMESTER_ID, -1);
		if (vid != -1) {
			return SemesterFragment.newInstance(vid);
		}
		return new SemesterFragment();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
