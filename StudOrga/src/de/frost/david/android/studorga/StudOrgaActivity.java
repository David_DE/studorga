package de.frost.david.android.studorga;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import de.frost.david.android.studorga.R;

public class StudOrgaActivity extends ActionBarActivity {
	public static final int REQUEST_VERANSTALTUNG = 1;
	public static final int REQUEST_STUNDE = 2;
	public static final int REQUEST_TERMIN = 3;
	public static final int UPDATED_SELECTED_SEMESTER = 4;

	private static final String FRAGMENT_STUNDENPLAN = "FRAGMENT_STUNDENPLAN";
	private static final String FRAGMENT_AUFGABENLISTE = "FRAGMENT_AUFGABENLISTE";
	private static final String FRAGMENT_TERMINLISTE = "FRAGMENT_TERMINLISTE";
	
	
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	private CharSequence mTitle;
	private String[] mMenuEintraege;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mTitle = getTitle();
		mMenuEintraege = getResources().getStringArray(R.array.menu_eintraege);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);

		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		// set up the drawer's list view with items and click listener
		mDrawerList.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, mMenuEintraege));
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		// enable ActionBar app icon to behave as action to toggle nav drawer
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		// ActionBarDrawerToggle ties together the the proper interactions
		// between the sliding drawer and the action bar app icon
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer, R.string.drawer_open, R.string.drawer_close) {
			@Override
			public void onDrawerClosed(View view) {
				getSupportActionBar().setTitle(mTitle);
				 supportInvalidateOptionsMenu();
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				getSupportActionBar().setTitle(getString(R.string.app_name));
				 supportInvalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			chooseFragment(0);
		}

	}

	/* Wird von (support)invalidateOptionsMenu() aufgerufen 
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// Wenn der Drawer offen ist, sollen keine Men� Items angezeigt werden
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);

		if (menu.findItem(R.id.menu_item_new) != null) {
			menu.findItem(R.id.menu_item_new).setVisible(!drawerOpen);
		} else if (menu.findItem(R.id.menu_item_new_termin) != null) {
			menu.findItem(R.id.menu_item_new_termin).setVisible(!drawerOpen);
		}
		return super.onPrepareOptionsMenu(menu);
	}

	/*
	 * Verarbeitet das Druecken von Menue eintraegen.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// The action bar home/up action should open or close the drawer.
		// ActionBarDrawerToggle will take care of this.

		if (item.getItemId() == android.R.id.home) {

			if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
				mDrawerLayout.closeDrawer(mDrawerList);
			} else {
				mDrawerLayout.openDrawer(mDrawerList);
			}
		}

		return super.onOptionsItemSelected(item);
	}

	/* The click listner for ListView in the navigation drawer */
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			if (position != 3) {
				chooseFragment(position);				
			} else { //Settings
				mDrawerLayout.closeDrawer(mDrawerList);
				startActivityForResult(new Intent(StudOrgaActivity.this, EinstellungenPreferenceActivity.class), UPDATED_SELECTED_SEMESTER);
			}
		}
	}

	/*
	 * Erstellt ein neues Fragment Objekt (je nachdem welcher Menuepunkt gedrueckt wurde)
	 * und tauscht das aktuelle Fragment mit dem neuen aus.
	 */
	private void chooseFragment(int position) {
		Fragment fragment = null;
		String tag = null;
		
		if (position == 0) { // Stundenplan
			fragment = new StundenplanTabFragment();
			tag = FRAGMENT_STUNDENPLAN;
		} else if (position == 1) { // Aufgaben
			fragment = new AufgabenListFragment();
			tag = FRAGMENT_AUFGABENLISTE;
		} else if (position == 2) { // Termine
			fragment = new TerminListFragment();
			tag = FRAGMENT_TERMINLISTE;
		}

		FragmentManager fragmentManager = getSupportFragmentManager();
		/* R.id.content_frame ist die ID des Framelayouts (in res/layout/activity_main.xml).
		 * Dieses "hostet" das Fragment
		 */	
		fragmentManager.beginTransaction().replace(R.id.content_frame, fragment, tag).commit();

		// update selected item and title, then close the drawer
		mDrawerList.setItemChecked(position, true);
		setTitle(mMenuEintraege[position]);
		mDrawerLayout.closeDrawer(mDrawerList);
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getSupportActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (resultCode == ActionBarActivity.RESULT_OK) {
			switch (requestCode) {
			case REQUEST_TERMIN:
				TerminListFragment tlf = (TerminListFragment)getSupportFragmentManager().findFragmentByTag(FRAGMENT_TERMINLISTE);
				tlf.updateUI();
				
				break;
			case UPDATED_SELECTED_SEMESTER: //Attach/Deatch um redraw zu erzwingen
				Log.i("test", "in UPDATE SELECTED SEMESTEr");
				StundenplanTabFragment temp = (StundenplanTabFragment)getSupportFragmentManager().findFragmentByTag(FRAGMENT_STUNDENPLAN);
				getSupportFragmentManager();
				FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
				transaction.detach(temp);
				transaction.attach(temp);
				transaction.commit();
				break;				
			case REQUEST_STUNDE:
				StundenplanTabFragment sptf = (StundenplanTabFragment)getSupportFragmentManager().findFragmentByTag(FRAGMENT_STUNDENPLAN);
				sptf.updateUI();
				
				break;
			case REQUEST_VERANSTALTUNG:
				//do nothing
				
				break;
			default:
				break;
			}
		}
	}
}