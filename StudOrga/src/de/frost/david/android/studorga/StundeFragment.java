package de.frost.david.android.studorga;

import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import de.frost.david.android.studorga.R;
import de.frost.david.android.studorga.lib.FunkLib;
import de.frost.david.android.studorga.model.IUniDataModel;
import de.frost.david.android.studorga.model.Stunde;
import de.frost.david.android.studorga.model.UniDataModelFactory;
import de.frost.david.android.studorga.model.Veranstaltung;
import de.frost.david.android.studorga.model.UniDatabaseHelper.VeranstaltungCursor;

public class StundeFragment extends Fragment {

	private EditText mStundeArtEditText;
	private EditText mStundeRaumEditText;
	private EditText mStundeDozentEditText;
	private Spinner mTagSpinner;
	private Spinner mVeranstaltungSpinner;

	private TimePickerDialog mUhrzeitStartDialog;
	private TimePickerDialog mUhrzeitEndDialog;
	private Button mSpeichernButton;
	private Button mUhrzeitStartButton;
	private Button mUhrzeitEndeButton;
	private int mStartHour = 8, mEndHour = 9, mStartMinute = 0, mEndMinute = 30;

	public static final String NEW_STUNDE = "de.thm.swtp.android.studorga.NEW_STUNDE";
	private static final String ARG_STUNDE_ID = "STUNDE_ID";
	
	private IUniDataModel mDataModel;
	private Stunde mStunde;
	
	private VeranstaltungCursor mCursor;

	public static Fragment newInstance(long sid) {
		Bundle args = new Bundle();
		args.putLong(ARG_STUNDE_ID, sid);
		StundeFragment sf = new StundeFragment();
		sf.setArguments(args);
		return sf;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		mDataModel = UniDataModelFactory.createInstance(getActivity());

		Bundle args = getArguments();
		if (args != null) {
			long id = args.getLong(ARG_STUNDE_ID, -1);
			if (id != -1) {
				mStunde = mDataModel.getStunde(id);
			}
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		((ActionBarActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
		((ActionBarActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		getActivity().setTitle(getString(R.string.add_new_stunde));
		/*
		 * res/layout/fragment_form_stunden beinhaltet das Layout, welches
		 * dieses Fragment benutzt. Dort wurden verschiedene View Objekte wie
		 * TextFelder, Labels, etc "angelegt" und je mit einer ID versehen.
		 */
		View v = inflater.inflate(R.layout.fragment_form_stunden, container, false);

		setHasOptionsMenu(true);

		// Ueber das View Objekt und der ID kann auf die einzelnen Objekte
		// zugegriffen werden
		mSpeichernButton = (Button) v.findViewById(R.id.stundenFormular_speichern_Button);
		mSpeichernButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				setStundeResultAndGo();

			}
		});
		

		mStundeArtEditText = (EditText) v.findViewById(R.id.stundenFormular_art_EditText);
		mStundeRaumEditText = (EditText) v.findViewById(R.id.stundenFormular_raum_EditText);
		mStundeDozentEditText = (EditText) v.findViewById(R.id.stundenFormular_dozent_EditText);

		// TODO: Testen/Abfangen, was ist, wenn noch keine Veranstaltung
		// verf�gbar! (ggf. gar nicht erst NewStundeActivity starten)
		
		mVeranstaltungSpinner = (Spinner) v.findViewById(R.id.stundenFormular_veranstaltungen_spinner);
		
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
		long sid = Long.parseLong(settings.getString("selectedSemester", "" + mDataModel.getLatestSemesterId()));
		mCursor = mDataModel.getAlleVeranstaltungenInSemester(sid);
		
		VeranstaltungCursorAdapter veradapter = new VeranstaltungCursorAdapter(getActivity(), mCursor);
		mVeranstaltungSpinner.setAdapter(veradapter);

		mTagSpinner = (Spinner) v.findViewById(R.id.stundenFormular_tage_spinner);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.wochentage, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTagSpinner.setAdapter(adapter);
		
		if (mStunde != null) {
			mStundeArtEditText.setText(mStunde.getArt());
			mStundeRaumEditText.setText(mStunde.getRaum());
			mStundeDozentEditText.setText(mStunde.getDozent());
			
			mStartHour = mStunde.getStart_stunde();
			mStartMinute = mStunde.getStart_minute();
			mEndHour = mStunde.getEnd_stunde();
			mEndMinute = mStunde.getEnd_minute();
			
			mTagSpinner.setSelection(mStunde.getWochentag());

			VeranstaltungCursorAdapter a = (VeranstaltungCursorAdapter) mVeranstaltungSpinner.getAdapter();
			
			for (int i = 0; i < a.getCount(); i++) {
				VeranstaltungCursor vc = (VeranstaltungCursor) a.getItem(i);
				
				if (vc.getVeranstaltung().getId() == mStunde.getVeranstaltungId()) {
					mVeranstaltungSpinner.setSelection(i);
					break;
				}
			}
			
			
		}
		
		mUhrzeitStartDialog = new TimePickerDialog(getActivity(), new TimePickHandler(true), mStartHour, mStartMinute, true);
		mUhrzeitEndDialog = new TimePickerDialog(getActivity(), new TimePickHandler(false), mEndHour, mEndMinute, true);

		mUhrzeitStartButton = (Button) v.findViewById(R.id.stundenFormular_ZeitStart_Button);
		mUhrzeitStartButton.setText(getText(R.string.StundeFragment_startzeit_button) + " (" + FunkLib.convert(mStartHour) + ":" + FunkLib.convert(mStartMinute) + ")");
		mUhrzeitStartButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mUhrzeitStartDialog.show();
			}
		});

		mUhrzeitEndeButton = (Button) v.findViewById(R.id.stundenFormular_ZeitEnd_Button);
		mUhrzeitEndeButton.setText(getText(R.string.StundeFragment_endzeit_button) + " (" + FunkLib.convert(mEndHour) + ":" + FunkLib.convert(mEndMinute) + ")");
		mUhrzeitEndeButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mUhrzeitEndDialog.show();
			}
		});

		return v;
	}

	/*
	 * setStundeResultAndGo ueberprueft, ob alle Felder Eingaben beinhalten und
	 * speicher diese in ein Stunde Objekt, welches in einem Intent der
	 * Acitivt�t "angehaengt" wird
	 */
	private void setStundeResultAndGo() {
		String art = mStundeArtEditText.getText().toString();
		String raum = mStundeRaumEditText.getText().toString();
		String dozent = mStundeDozentEditText.getText().toString();
		int tag = mTagSpinner.getSelectedItemPosition();

		if (art.equals("")) {
			Toast.makeText(getActivity(), "Art Textfeld ist nicht ausgef�llt worden!", Toast.LENGTH_SHORT).show();
			return;
		} else if (raum.equals("")) {
			Toast.makeText(getActivity(), "Raum Textfeld ist nicht ausgef�llt worden!", Toast.LENGTH_SHORT).show();
			return;
		} else if (dozent.equals("")) {
			Toast.makeText(getActivity(), "Dozent Textfeld ist nicht ausgef�llt worden!", Toast.LENGTH_SHORT).show();
			return;
		}

		Intent data = new Intent();
		
		if (mStunde != null) {
			mStunde.setArt(art);
			mStunde.setRaum(raum);
			mStunde.setDozent(dozent);
			mStunde.setStart_stunde(mStartHour);
			mStunde.setStart_minute(mStartMinute);
			mStunde.setEnd_stunde(mEndHour);
			mStunde.setEnd_minute(mEndMinute);
			mStunde.setWochentag(tag);
			mStunde.setVeranstaltungId(((VeranstaltungCursor)mVeranstaltungSpinner.getSelectedItem()).getVeranstaltung().getId());
			mDataModel.updateStunde(mStunde);
		} else {
			mStunde = new Stunde(art, raum, tag, dozent, mStartHour, mStartMinute, mEndHour, mEndMinute);
			mStunde.setVeranstaltungId(((VeranstaltungCursor)mVeranstaltungSpinner.getSelectedItem()).getVeranstaltung().getId());
			mStunde.setId(mDataModel.insertStunde(mStunde));
		}
		
		data.putExtra(NEW_STUNDE, mStunde.getWochentag());
		getActivity().setResult(ActionBarActivity.RESULT_OK, data);

		getActivity().finish();

	}

	class TimePickHandler implements OnTimeSetListener {
		private boolean start;

		public TimePickHandler(boolean start) {
			this.start = start;
		}

		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			if (start) {
				mStartHour = hourOfDay;
				mStartMinute = minute;
				mUhrzeitStartButton.setText(getText(R.string.StundeFragment_startzeit_button) + " (" + FunkLib.convert(hourOfDay) + ":" + FunkLib.convert(minute) + ")");
				mUhrzeitStartDialog.hide();
			} else {
				mEndHour = hourOfDay;
				mEndMinute = minute;
				mUhrzeitEndeButton.setText(getText(R.string.StundeFragment_endzeit_button) + " (" + FunkLib.convert(hourOfDay) + ":" + FunkLib.convert(minute) + ")");
				mUhrzeitEndDialog.hide();
			}

		}

	}


	public static class VeranstaltungCursorAdapter extends CursorAdapter {
		
		private VeranstaltungCursor mVeranstaltungCursor;

		public VeranstaltungCursorAdapter(Context context, VeranstaltungCursor c) {
			super(context, c, 0);
			this.mVeranstaltungCursor = c;
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			return inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
		}
		
		@Override
		public void bindView(View v, Context c, Cursor cursor) {
			Veranstaltung ver = mVeranstaltungCursor.getVeranstaltung();
			
			TextView tv = (TextView)v;
			tv.setText(ver.getName());
		}

		
	}

}
