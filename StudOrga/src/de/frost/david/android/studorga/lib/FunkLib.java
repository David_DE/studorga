package de.frost.david.android.studorga.lib;

import java.util.Calendar;

public class FunkLib {
	
	public static String convert(int number, int digit) {
	    String buffer = String.valueOf(number);
	    while(buffer.length() != digit)
	        buffer="0" + buffer;
	    return buffer;
	}
	
	/*
	 * Gibt eine Zahl als String zuruck mit vorangestellten Nullen,
	 * falls die Zahl kleiner als Zweistellig ist
	 */
	public static String convert(int number) {
		String buffer = String.valueOf(number);
		while(buffer.length() != 2)
			buffer="0" + buffer;
		return buffer;
	}
	
	public static int getHour() {
		Calendar c = Calendar.getInstance();
		return c.get(Calendar.HOUR_OF_DAY);
	}
	
	public static int getMinute() {
		Calendar c = Calendar.getInstance();
		return c.get(Calendar.MINUTE);
	}
	
	public static int getDay() {
		Calendar c = Calendar.getInstance();
		return c.get(Calendar.DAY_OF_MONTH);
	}
	
	/**
	 * @return liefert einen Integer zwischen 1 und 12
	 */
	public static int getMonth() {
		Calendar c = Calendar.getInstance();
		return c.get(Calendar.MONTH)+1; 
	}
	
	public static int getYear() {
		Calendar c = Calendar.getInstance();
		return c.get(Calendar.YEAR); 
	}

	public static int getDayOfWeek() {
		Calendar c = Calendar.getInstance();
		return c.get(Calendar.DAY_OF_WEEK);
	}
	
	public static String getSemesterName() {
		String temp="";
		
		if(getMonth() >= 5) {
			temp+="WS ";
			String year1 = ""+getYear();
			String year2 = ""+(getYear()+1);
			year1 = (String) year1.subSequence(2, year1.length());
			year2 = (String) year2.subSequence(2, year2.length());
			
			temp+=year1+"/"+year2;
		} else {
			temp +="SS " + getYear();
		}
		
		return temp;
	}

}
