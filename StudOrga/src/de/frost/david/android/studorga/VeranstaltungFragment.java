package de.frost.david.android.studorga;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import de.frost.david.android.studorga.R;
import de.frost.david.android.studorga.model.IUniDataModel;
import de.frost.david.android.studorga.model.Semester;
import de.frost.david.android.studorga.model.UniDataModelFactory;
import de.frost.david.android.studorga.model.Veranstaltung;
import de.frost.david.android.studorga.model.UniDatabaseHelper.SemesterCursor;

public class VeranstaltungFragment extends Fragment {

	private EditText mVeranstaltungNameEditText;
	private EditText mVeranstaltungAbkEditText;
	private EditText mVeranstaltungCreditsEditText;
	private Spinner mSemesterSpinner;

	private Button mSpeichernButton;

	public static final String NEW_VERANSTALTUNG = "de.thm.swtp.android.studorga.NEW_VERANSTALTUNG";
	private static final String ARG_VERANSTALTUNG_ID = "VERANSTALTUNG_ID";
	
	private IUniDataModel mDataModel;
	private Veranstaltung mVeranstaltung;
	
	private SemesterCursor mCursor;

	public static Fragment newInstance(long sid) {
		Bundle args = new Bundle();
		args.putLong(ARG_VERANSTALTUNG_ID, sid);
		VeranstaltungFragment vf = new VeranstaltungFragment();
		vf.setArguments(args);
		return vf;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		mDataModel = UniDataModelFactory.createInstance(getActivity());

		Bundle args = getArguments();
		if (args != null) {
			long id = args.getLong(ARG_VERANSTALTUNG_ID, -1);
			if (id != -1) {
				mVeranstaltung = mDataModel.getVeranstaltung(id);
			}
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		((ActionBarActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
		((ActionBarActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		getActivity().setTitle(getString(R.string.add_new_veranstaltung));
		View v = inflater.inflate(R.layout.fragment_form_veranstaltung, container, false);

		setHasOptionsMenu(true);

		mSpeichernButton = (Button) v.findViewById(R.id.veranstaltungFormular_speichern_Button);
		mSpeichernButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				setVeranstaltungResultAndGo();

			}
		});
		

		mVeranstaltungNameEditText = (EditText) v.findViewById(R.id.veranstaltungFormular_name_EditText);
		mVeranstaltungAbkEditText = (EditText) v.findViewById(R.id.veranstaltungFormular_abk_EditText);
		mVeranstaltungCreditsEditText = (EditText) v.findViewById(R.id.veranstaltungFormular_credits_EditText);

		
		mSemesterSpinner = (Spinner) v.findViewById(R.id.veranstaltungFormular_semester_spinner);
		mCursor = mDataModel.getAlleSemester();
		SemesterCursorAdapter seradapter = new SemesterCursorAdapter(getActivity(), mCursor);
		mSemesterSpinner.setAdapter(seradapter);

		
		if (mVeranstaltung != null) {
			mVeranstaltungNameEditText.setText(mVeranstaltung.getName());
			mVeranstaltungAbkEditText.setText(mVeranstaltung.getAbkuerzung());
			mVeranstaltungCreditsEditText.setText(""+mVeranstaltung.getCredits());
			

			SemesterCursorAdapter a = (SemesterCursorAdapter) mSemesterSpinner.getAdapter();
			
			for (int i = 0; i < a.getCount(); i++) {
				SemesterCursor sc = (SemesterCursor) a.getItem(i);
				
				if (sc.getSemester().getId() == mVeranstaltung.getSemesterId()) {
					mSemesterSpinner.setSelection(i);
					break;
				}
			}
		}
		
		return v;
	}

	private void setVeranstaltungResultAndGo() {
		String name = mVeranstaltungNameEditText.getText().toString();
		String abk = mVeranstaltungAbkEditText.getText().toString();
		String credits = mVeranstaltungCreditsEditText.getText().toString();

		if (name.equals("")) {
			Toast.makeText(getActivity(), getString(R.string.VeranstaltungFragment_fehler_name), Toast.LENGTH_SHORT).show();
			return;
		} else if (abk.equals("")) {
			Toast.makeText(getActivity(), getString(R.string.VeranstaltungFragment_fehler_abk), Toast.LENGTH_SHORT).show();
			return;
		} else if (credits.equals("")) {
			Toast.makeText(getActivity(), getString(R.string.VeranstaltungFragment_fehler_credits), Toast.LENGTH_SHORT).show();
			return;
		}

		Intent data = new Intent();
		
		if (mVeranstaltung != null) {
			mVeranstaltung.setName(name);
			mVeranstaltung.setAbkuerzung(abk);
			mVeranstaltung.setCredits(Integer.parseInt(credits));
			mVeranstaltung.setSemesterId(((SemesterCursor)mSemesterSpinner.getSelectedItem()).getSemester().getId());
			mDataModel.updateVeranstaltung(mVeranstaltung);
		} else {
			mVeranstaltung = new Veranstaltung(name, abk);
			mVeranstaltung.setCredits(Integer.parseInt(credits));
			mVeranstaltung.setSemesterId(((SemesterCursor)mSemesterSpinner.getSelectedItem()).getSemester().getId());
			mVeranstaltung.setId(mDataModel.insertVeranstaltung(mVeranstaltung));
		}
		
		getActivity().setResult(ActionBarActivity.RESULT_OK, data);

		getActivity().finish();

	}


	public static class SemesterCursorAdapter extends CursorAdapter {
		
		private SemesterCursor mSemesterCursor;

		public SemesterCursorAdapter(Context context, SemesterCursor c) {
			super(context, c, 0);
			this.mSemesterCursor = c;
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			return inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
		}
		
		@Override
		public void bindView(View v, Context c, Cursor cursor) {
			Semester s = mSemesterCursor.getSemester();
			
			TextView tv = (TextView)v;
			tv.setText(s.getName());
		}

		
	}

}
